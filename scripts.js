// CURRENT DATE
let today = new Date().toISOString().slice(0, 10)
document.getElementById("current__date").innerHTML = today;

// VARIABLES TO COUNT INCOME SUM
var totalIncomeAmount = [];
var sum = 0;

// VARIABLES TO COUNT EXPENSES SUM
var totalExpencesAmount = [];
var sum2 = 0;

// MONTH BALANCE VAR
var monthBalance = 0;

// GETTING INCOME AND EXPENCES ID BY CLICK
var incomeExpencesId = '';

function getIncExpId(id) {
    incomeExpencesId = id;
	console.log(incomeExpencesId);	
}

function addMoney() {
	
	if (incomeExpencesId == "inc") {
		
		// DISPLAY INCOME LIST NAMES
		var item = document.getElementById("income__expences--input").value;
		var text = document.createTextNode(item);
		var newItem = document.createElement("div");
		newItem.appendChild(text);
		document.getElementById("income__list--item--name").appendChild(newItem);

		// DISPLAY INCOME LIST
		var item1 = document.getElementById("income__expences--amount").value; 
		var text1 = document.createTextNode(item1);
		var newItem1 = document.createElement("div");
		newItem1.appendChild(text1);
		document.getElementById("income__list--amount").appendChild(newItem1);

		// SUM OF THE INCOME
	    sum = 0;
		totalIncomeAmount.push(parseInt(item1));
		for (var i = 0; i < totalIncomeAmount.length; i++) {
			sum += totalIncomeAmount[i];
		}
		console.log(sum);  
		
		// DISPLAY SUM OF INCOME
		document.getElementById("income--amount").innerHTML = sum;

		
	} else if (incomeExpencesId == "exp") {
		
		// DISPLAY EXPENSES LIST NAMES
		var item2 = document.getElementById("income__expences--input").value;
		var text2 = document.createTextNode(item2);
		var newItem2 = document.createElement("div");
		newItem2.appendChild(text2);
		document.getElementById("expences__list--item--name").appendChild(newItem2);

		// DISPLAY EXPENSES LIST
		var item3 = document.getElementById("income__expences--amount").value;
		var text3 = document.createTextNode(item3);
		var newItem3 = document.createElement("div");
		newItem3.appendChild(text3);
		document.getElementById("expences__list--amount").appendChild(newItem3);
		
		// SUM OF THE EXPENSES
	    sum2 = 0;
		totalExpencesAmount.push(parseInt(item3));
		for (var i = 0; i < totalExpencesAmount.length; i++) {
			sum2 += totalExpencesAmount[i];
		}
		console.log(sum2);  
		
		// DISPLAY SUM OF EXPENSES
		document.getElementById("expenses__amount").innerHTML = sum2;
		
	}
	
	// MONTH BALACE COUNTER
	monthBalance = sum - sum2;
	
	// DISPLAY MONTH BALACE 
	document.getElementById("monthly__gain").innerHTML = monthBalance;
	
	
}









